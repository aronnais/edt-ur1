//libraries
var _ = require('underscore');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var moment = require('moment');
var ical = require('ical');
var https = require("https");
var fs = require('fs');

// create application/json parser
var jsonParser = bodyParser.json()

// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({
	extended: false
})

var mapping = require(__dirname + '/files/mapping.json');

var arr = Object.keys(mapping);

var promo_list = [];
arr.forEach(function(element, index) {
	promo_list.push({
		name: mapping[element].name,
		url: '#/' + element
	});
});

var last_cached = new Date();
var cache = {};
var cache_delay = 1000 * 60 * 60 * 2; //2 heures
var cache_delay_hours = 2;
var version = "3.0.1";

var file_storage = __dirname + '/files/';

//CONFIG
app.set('port', (process.env.PORT || 5000));
app.set('json spaces', 4);

//CORS
app.all('/*', (req, res, next) => {
	res.header("Access-Control-Allow-Origin", "*");
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
	next();
});

app.get('/promo_list', jsonParser, (request, response) => {
	response.json(mapping);
});

app.post('/ical', jsonParser, (request, response, next) => {

	var id = request.body.promo;

	var promo = _.find(mapping, (prom) => {
		return prom.id == id;
	});

	var cached = null;

	if( !promo ){
		response.json({ success:false, error:{ code : 101, message : 'Invalid promo ID' }});
		return;
	}

	//charger la version en cache
	try {
		cached = require(file_storage + 'cache/' + promo.id + '.json');
	} catch (ex) {
		cached = false;
	}

	if (cached) {
		console.log('Cached version found');
		var now = moment();
		var then = moment(cached.lastUpdate, 'DD-MM-YYYY HH:mm');

		var duration = moment.duration(now.diff(then));
		var old = duration.asHours();

		console.log('Version is ' + old + ' hours old');

		if (old > cache_delay_hours) {
			console.log("version is too old ! need redownload ...")
		} else {

			response.json({
				success: true,
				data: cached.data
			});

			return;
		}

	}

	console.log('No cached version found');

	url = promo.url;

	check_connexion(url, response, function(up) {
		if (up) {
			ical.fromURL(url, {}, function(err, data) {
				var result = [];

				if (err) {
					console.log('ERROR : Unable to get data from server');
				} else {
					var planning = {};

					for (var k in data) {
						if (data.hasOwnProperty(k)) {
							var ev = data[k];

							ev.description = ev.description.replace(/(\(Exporté le.*\))/gi, '');

							var date = new Date(ev.start);

							var key = moment(date).format("YYYY-MM-DD");

							//key exists
							if (planning[key]) {
								planning[key].events.push(ev);
							} else {
								planning[key] = {
									date: key,
									events: [ev]
								};
							}

						}
					}


					for (var day in planning) {
						result.push(planning[day]);
					}
				}

				var cached = {
					lastUpdate: moment().format("DD-MM-YYYY HH:mm"),
					data: result
				};

				var file = file_storage + 'cache/' + promo.id + '.json';
				fs.writeFile(file, JSON.stringify(cached, null, 4), function(err) {
					if (err) {
						console.log(err);
					} else {
						console.log("Backup saved to " + file);
					}
				});

				//on met la requette en cache si jamais on nous re-demande cette promo
				response.json({
					success: true,
					data: result
				});

			 	return;
			});

		} else {

			//l'ent est mort :/
			response.json({
				success: false,
				error: {
					code: 100,
					message: 'Server is not responding '
				}
			});
			return;
		}
	})

});

const CHECK_CONNECTION_TIMEOUT = 10 * 1000;

//FUNCTIONS
function check_connexion(url, response, _callback) {

	console.log('verification de la connexion ...');

	var req = https.get(url, (res) => {
		console.log('UP');
		_callback(res.statusCode != 408);

	})

	req.on('error', (e) => {
		_callback(false);
	})

	req.on('socket', (socket) => {
		socket.setTimeout(CHECK_CONNECTION_TIMEOUT);
		socket.on('timeout', function() {
			console.log('TIMEOUT !');
			req.abort();
		});
	});
}

//WEB VIEW
app.use(express.static(__dirname + '/public/v3'));

app.get('/', urlencodedParser, function(request, response) {
	response.sendfile('public/v3/index.html');
});

//SERVICE STATUS
app.get('/status', urlencodedParser, function(request, response) {
	response.sendStatus(200);
});

//SERVICE VERSION
app.get('/version', urlencodedParser, function(request, response) {
	response.send(version);
});

//DEFAULT ROUTE
app.use(function(request, response) {
	response.json({
		success: false,
		message: 'invalid request'
	});
})

//START APP
app.listen(app.get('port'), function() {
	console.log('Node app is running on port', app.get('port'));
});