#Api pour application calendar

#[Site](http://edt-ur1.herokuapp.com/)

##Routes
- GET /cal -> retourne un tableau json représentant une semaine de cours a compter du jour d'éxécution de la requette
	- la requette doit contenir le paramètre api_key contennant la valeur de la clé d'api

- GET /status -> retourne "ok" si le service est en marche
- GET /version : Retourne la version en ligne

##Déploiement
- Faire des push sur la branche developement
- Merger vers master et au bout de 2-3 minutes le projet est redéployé sur Heroku en passant par CodeShift

## TODO
- a faire :)