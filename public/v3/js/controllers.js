//var API_URL = "http://edt-ur1.herokuapp.com";
var PROMOS = {};
var CURRENT = "";

angular.module('starter.controllers', [])

.controller('AppCtrl', function($state, $scope, $ionicModal, $timeout, $http, $localstorage) {


  $scope.promos = PROMO_LIST;

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/search_modal.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeSearch = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.openSearch = function() {
    $scope.modal.show();
  };

  $scope.update = function(){

    $localstorage.clear();
    $state.go('app.home')
  }


})

.controller('HomeCtrl', function($scope, $localstorage) {

  $scope.favoris = $localstorage.getObject("favoris") || {};

  $scope.favoris = [{
    name: 'L3 MIAGE',
    id: "l3miage",
  },{
    name: 'M1 MIAGE',
    id: "m1miage",
  },{
    name: 'M2 MIAGE',
    id: "m2miage",
  }];

})

.controller('CalendarCtrl', function($state, $scope, $stateParams, $cacheSystem, $timeout) {
  $scope.initCalendar = function(events) {

    var evts = [];

    _.each(events, function(day) {

      _.each(day.events, function(e) {
        evts.push({
          start: e.start,
          end: e.end,
          title: e.summary
        });
      });
    });

    $('#calendar').fullCalendar({
      events: evts,
      weekends: false,
      height: 700,
      minTime: '07:00',
      maxTime: '20:00',
      lang: "fr",
      allDaySlot: false,
      firstDay: 1,
      header: {
        left: 'title',
        center: '',
        right: 'today prev,next'
      },
      defaultView: "agendaDay"
    });
  }

  $scope.loadCalendar = function(id) {

    console.log("Show promo " + id);

    $scope.promo = CURRENT;

    if (CURRENT) {
      id = CURRENT.id;
    } else {
      $state.go('app.home')
    }

    var val = _.find(PROMO_LIST, function(prom) {
      return prom.id == id;
    });

    if (val) {
      $scope.promo.name = val.name;

      $cacheSystem.LoadEvents('promo', id, false, function(events) {

        $timeout(function() {
          $('#calendar').fullCalendar('destroy');
          console.log("update agenda for " + id)
          $scope.initCalendar(events.data);
        }, 250);

      });
    } else {}
  }


  $scope.$on('$ionicView.enter', function(e) {

    var id = $stateParams.promoId;

    $scope.loadCalendar(id);
  });
})

.controller('PromoCtrl', function($state, $scope, $stateParams, $cacheSystem, $timeout) {


  $scope.initCalendar = function(events) {

    function randomColor() {
      var cols = ['#d01716', '#c2185b', '#7b1fa2', '#512da8', '#303f9f', '#455ede', '#0288d1', '#0097a7', '#00796b', '#0a7e07', '#689f38', '#afb42b', '#fbc02d', '#f57c00', '#e64a19', '#5d4037', '#616161', '#455a64'];

      return cols[Math.floor(Math.random() * cols.length)];
    }

    var evts = [];

    _.each(events, function(day) {

      _.each(day.events, function(e) {
        evts.push({
          start: e.start,
          end: e.end,
          title: e.summary,
          location : e.location,

          color: randomColor()
        });
      });
    });

    $('#calendar.' + $scope.promo.id + '').fullCalendar({
      events: evts,
      eventRender: function(event, element) {
        $(element).append('<p class="fc-location"> '+event.location+'</p>');
      },
      timezone: 'local',
      allDaySlot: false,
      editable: false, // Don't allow editing of events
      contentHeight: 'auto',
      weekends: false, // Hide weekends
      defaultView: 'agendaDay', // Only show week view
      header: {
        left: 'title',
        center: '',
        right: 'today prev,next'
      },
      minTime: '07:30:00', // Start time for the calendar
      maxTime: '20:30:00', // End time for the calendar
      columnFormat: {
        week: 'ddd' // Only show day of the week names
      },
    });
  }

  $scope.loadCalendar = function(id) {

    console.log("Show promo " + id);

    var val = _.find(PROMO_LIST, function(prom) {
      return prom.id == id;
    });

    if (val) {
      $scope.promo = val;

      $cacheSystem.LoadEvents('promo', id, false, function(events) {

        $scope.events = events;

        $timeout(function() {
          $scope.initCalendar(events.data);
        }, 250);

      });
    } else {
      alert("La promo '" + id + "'' n'existe pas");
    }
  }



  var id = $stateParams.promoId;

  $scope.loadCalendar(id);

});