var API_URL = "http://edt-ur1.herokuapp.com";
//var API_URL = "http://localhost:5000";

var PROMO_LIST = [];
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic','ionic.service.core', 'starter.controllers', 'starter.services'])
.constant('ApiEndpoint', {
  url: 'http://edt-ur1.herokuapp.com'
})
.run(function($ionicPlatform, $localstorage, $cacheSystem, $http) {


    var cache_promos = $localstorage.getObject('promo_list');


    //console.log(cache_promos);

    var expiration =  (new Date( (new Date(cache_promos.date)).getTime() + 2 * 60 * 60 * 1000)).getTime();

    //console.log('expires at ' + expiration);
    //console.log('not is  ' + (new Date()).getTime());

    var expired = true;

    if ( (new Date()).getTime() > expiration){
      //console.log('Expired !');
      expired = true;
    }else{
      //console.log('not Expired !');
      expired = false;
    }


    if( isEmpty(cache_promos) || expired){
      //alert('promo_list not in cache');
      
      $http.get(API_URL + '/promo_list').then(function(res){
        if(res.status == 200){
          PROMO_LIST = res.data;

          var cache_promos = {
            date : new Date(),
            data : PROMO_LIST
          }

          $localstorage.setObject('promo_list', cache_promos);

          //alert('promo_list loaded');
        }else{
          //alert('promo_list failed to fetch');
        }
      });

    }else{
      PROMO_LIST = cache_promos.data;
      //alert('promo_list in cache');
    }
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })


  .state('app.calendar', {
      url: '/calendar',
      views: {
        'menuContent': {
          templateUrl: 'templates/calendar.html',
          controller: 'CalendarCtrl'
        }
      }
    })

  .state('app.promo', {
      url: '/promo/:promoId',
      views: {
        'menuContent': {
          templateUrl: 'templates/promo.html',
          controller: 'PromoCtrl'
        }
      }
    })
    .state('app.home', {
      url: '/home',
      views: {
        'menuContent': {
          templateUrl: 'templates/home.html',
          controller: 'HomeCtrl'
        }
      }
    })
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
});


// Speed up calls to hasOwnProperty
var hasOwnProperty = Object.prototype.hasOwnProperty;

function isEmpty(obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}