angular.module('starter.services', [])

.factory('$localstorage', ['$window', function($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      return JSON.parse($window.localStorage[key] || '{}');
    },
    clear : function(){
      localStorage.clear();
    }
  }
}])

.factory('Api', function($http, ApiEndpoint) {
  console.log('ApiEndpoint', ApiEndpoint)

  var getApiData = function() {
    return $http.get(ApiEndpoint.url + '/tasks')
      .then(function(data) {
        console.log('Got some data: ', data);
        return data;
      });
  };

  return {
    getApiData: getApiData
  };
})

.factory('$cacheSystem', function($localstorage, $q, $http) {

  var api = "";
  return {
    LoadEvents: function( type, id, force, _callback) {

      var data = $localstorage.getObject('promo::' + id);
      //if(true){
      if(_.isEmpty(data) ){
        console.log('data not found auto rebuild ...');
        this.Rebuild( type, id,_callback);
      }else{

        var now = moment();
        var then = moment(data.lastUpdate);

        var duration = moment.duration(now.diff(then));
        var old = duration.asHours();

        console.log('version is ' + old + ' hours old');

        if(old > 2 || force){
          this.Rebuild( type, id,_callback);
        }else{
          console.log('cached version is recent');
          _callback(data);
        }
      }
    },
    Rebuild: function(type, id, _callback) {

      console.log('rebuild : ' + type + "::"+id);

      switch(type) {
        case "promo":

          var val = _.find(PROMO_LIST, function(prom){ return prom.id == id; });

          //demande au serveur le fichier ical parsé
          $http.post(API_URL +'/ical', {promo:id}).then(function(res) {

            var response = res.data;

            //le serveur nous rend les events sans problème
            if (response.success) {


              var events = {
                lastUpdate : new Date(),
                data : response.data
              }

              //enreg en local storage
              $localstorage.setObject('promo::' + id, events);

              _callback(events);
              
            } else {

              //le serveur envoie une erreur
              if(response.error && response.error.code){
                var err = response.error;
                alert(err.code);
              }
            }

          });
        break;
      }

    }
  }
})