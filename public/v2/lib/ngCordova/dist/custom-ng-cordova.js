/*!
 * ngCordova
 * v0.1.20-alpha
 * Copyright 2015 Drifty Co. http://drifty.com/
 * See LICENSE in this repository for license information
 */
(function(){

  angular.module('ngCordova', []);

  angular.module('main.ctrl',[]).controller('MainCtrl', function ($scope, $log) {

    $scope.loading = false;

    $scope.start = function(){
      $scope.loading = true;
    };

  });

})();