if (document.location.hostname == 'localhost') {
	console.log('LOCAL !');
	url = "http://localhost:5000";
} else {
	url = "http://edt-ur1.herokuapp.com";
}

var agendaControllers = angular.module('agendaControllers', []);

agendaControllers.controller('PromoCtrl', function($scope, $routeParams, $http, favorisManager,$filter) {

	var promoID = $routeParams.promoId;

	$("#page-title").html(promoID.toUpperCase());

	$("#favorites").removeClass('fav');

	$scope.next_day = function() {

		$('#previous_day_btn').removeClass('disabled');

		if ($scope.displayed_day < ($scope.day_list.length - 1)) {
			$scope.displayed_day++;
			$scope.current_date = $scope.day_list[$scope.displayed_day];
		}

		if ($scope.displayed_day >= ($scope.day_list.length - 1)) {

			$('#next_day_btn').addClass('disabled');
		}
	}

	$scope.previous_day = function() {

		$('#next_day_btn').removeClass('disabled');

		if ($scope.displayed_day > 0) {
			$scope.displayed_day--;
			$scope.current_date = $scope.day_list[$scope.displayed_day];
		}

		if ($scope.displayed_day <= 0) {
			$('#previous_day_btn').addClass('disabled');
		}
	}

	$scope.current_date = new Date();
	$scope.loading = true;
	$scope.show_app = false;
	$scope.displayed_day = 0;
	$scope.day_list = [];

	$scope.days = [];

	$scope.update_error = true;

	$scope.status = {
		state: 'loading',
		icon: 'sync',
		color: 'orangered'
	};

	//check all
	var req = {
		method: 'GET',
		url: url + "/cal/" + promoID
	};

	$http(req).then(function(res) {

		if (!res.data || !res.data.planning) return;

		if (!res.data.is_up_to_date) {
			$scope.error = {};
			$scope.error.color = 'orange';
			$scope.error.icon = 'warning';
			$scope.error.message = 'Le serveur est hors-ligne. Ceci est la dernière version en cache datant du ' + $filter('date')(new Date(res.data.last_updated), ' dd/MM/yyyy à HH:mm'); + '';
		}
		$scope.days = res.data.planning;

		for (var day in $scope.days) {
			var element = $scope.days[day];
			$scope.day_list.push(element.date);
		}

		$scope.day_list.sort();

		var today = (new Date()).toISOString().slice(0, 10);

		var index = $scope.day_list.indexOf(today);

		if (index == -1 || index >= $scope.day_list.length) {
			$scope.current_date = $scope.day_list[0];
			$scope.displayed_day = 0;
		} else {
			$scope.current_date = $scope.day_list[index];
			$scope.displayed_day = index;
		}

		if ($scope.displayed_day == 0) {
			$('#previous_day_btn').addClass('disabled');
		} else if ($scope.displayed_day == $scope.day_list.length) {
			$('#next_day_btn').addClass('disabled');
		}

		$scope.loading = false;

		//favoris
		current_promo = { name:res.data.promo, url:'#/'+promoID};

		$("#favorites").show();

		if(favorisManager.isInFavorites(current_promo)){
			$("#favorites").addClass('fav');
		}


		$scope.show_app = true;
		$scope.status.state = 'ready';

	}, function(err) {
		console.log('Error 2');
	});


});

agendaControllers.controller('NavController', ['$scope','$http','$routeParams', 'favorisManager',function($scope, $http, $routeParams, favorisManager) {

	$scope.liste_favoris = favorisManager.load();
	
	$scope.nb_favoris = Object.keys($scope.liste_favoris).length;

	$scope.add_favoris = function() {

		var res = favorisManager.update(current_promo);
		if(res == 0){
			$("#favorites").removeClass('fav');
			Materialize.toast('Favori supprimé', 3000, 'rounded')
		}

		if(res == 1){
			$("#favorites").addClass('fav');
			Materialize.toast('Ajouté aux favoris', 3000,'rounded')
		}

		$scope.nb_favoris = Object.keys($scope.liste_favoris).length;
	};

	$scope.promo_list = [];
	$http({
		method: 'GET',
		url: url + '/promo_list'
	}).then(function(res) {
		if (!res.data) return;

		$scope.promo_list = res.data;

	});
}]);

agendaControllers.controller('MainCtrl', function($scope, $http, $timeout, $filter) {

	$("#page-title").html("Agenda");

});