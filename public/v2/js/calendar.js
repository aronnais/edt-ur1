var app = angular.module('Calendar',['ngRoute', 'agendaControllers']);

app.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/', {
        templateUrl: 'views/home.html',
        controller: 'MainCtrl'
      }).
      when('/:promoId', {
        templateUrl: 'views/promo.html',
        controller: 'PromoCtrl'
      }).
      otherwise({
        redirectTo: '/'
      });
  }]);


app.filter("sanitize", ['$sce', function($sce) {
  return function(htmlCode){
    return $sce.trustAsHtml(htmlCode);
  }
}]);

app.factory('favorisManager', function() {
  
  return {
      list : null,
      load : function() {
        if(!this.list){
          this.list = JSON.parse(localStorage.getItem('favoris'));

          if (!this.list) {
            this.list = {};
            this.save();
          }
        }

        return this.list;
      },
      isInFavorites: function(elem){
        return (this.load()[elem.name] != null)
      },
      update: function(elem){
        //delete
        if(!elem){
          return -1;
        }

        if(this.list[elem.name]){
          delete this.list[elem.name];
          this.save();
          return 0;
        }else{
          this.list[elem.name] = elem;
          this.save();
          return 1;
        }

      },
      save : function(){
        localStorage.setItem('favoris', JSON.stringify(this.list));
      }
  }
});