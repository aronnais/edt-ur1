(function($){
  $(function(){
    $('.button-collapse').sideNav({
      menuWidth: 300,
      closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
    });

    $('.modal-trigger').leanModal();

    $('select').material_select();

    $('body').fadeIn(1000);

    $("body").on("swipe",function(){
      alert("swipe");
    });
  }); // end of document ready
})(jQuery); // end of jQuery name space