var app = angular.module('Calendar',[])

.controller('MainCtrl', function($scope, $http, $timeout, $filter){

	$scope.generatePlanning = function(data){

    
	};

	$scope.loader = function(bool){

		if(!bool)
			bool = false;

		$scope.loading = bool;
	}

  $scope.next_day = function(){

    document.getElementById('previous_day').removeAttribute('disabled');

    if($scope.displayed_day < ($scope.day_list.length-1)){
      $scope.displayed_day++;
      $scope.current_date = $scope.day_list[$scope.displayed_day];
    }
      
    if($scope.displayed_day >= ($scope.day_list.length-1)){
      document.getElementById('next_day').setAttribute('disabled','true');
    }
  }

  $scope.previous_day=function(){

    document.getElementById('next_day').removeAttribute('disabled');

    if($scope.displayed_day > 0){
      $scope.displayed_day--;
      $scope.current_date = $scope.day_list[$scope.displayed_day];
    }

    if($scope.displayed_day <= 0){
      document.getElementById('previous_day').setAttribute('disabled','true');
    }
  }

  if(document.location.hostname == 'localhost'){
    console.log('LOCAL !');
    url = "http://localhost:5000/";  
  }else{
    url = "http://edt-ur1.herokuapp.com/";  
  }

  api = url+"cal";  

	$scope.current_date = new Date();
	$scope.loading = true;
  $scope.show_app = false;
  $scope.displayed_day = 0;
  $scope.day_list = [];
  $scope.days = [];

  $scope.update_error = true;

  $scope.status = {
    state:'loading',
    icon:'sync',
    color:'orangered'
  };

	//check all
  var req = {method: 'GET',url: api}

  $http(req).then(function(res){

    if(!res.data || !res.data.planning) return;

    if(!res.data.is_up_to_date){
      $scope.status.color='orange';
      $scope.status.icon='warning';
      $scope.status.message='Le serveur est hors-ligne.<br/>Dernière MAJ : '+$filter('date')(res.data.last_updated, 'dd/M/yyyy HH:mm')+'';
    }else{
      $scope.status.color='white';
      $scope.status.icon='check';
      $scope.status.message='Tout est OK';
    }

    $scope.days = res.data.planning;

    for(var day in $scope.days){
      var element = $scope.days[day];
      $scope.day_list.push(element.date);
    }

    $scope.day_list.sort();

    var today = (new Date()).toISOString().slice(0,10);

    var index = $scope.day_list.indexOf(today);

    if(index == -1 || index >= $scope.day_list.length){
      $scope.current_date = $scope.day_list[0];
      $scope.displayed_day = 0;
    } else{
      $scope.current_date = $scope.day_list[index];
      $scope.displayed_day = index;
    }

    if($scope.displayed_day == 0){
      document.getElementById('previous_day').setAttribute('disabled','true');
    }else if($scope.displayed_day == $scope.day_list.length){
      document.getElementById('next_day').setAttribute('disabled','true');
    }

    $scope.show_app = true;
    $scope.status.state = 'ready';
    

  }, function(err){
    console.log('Error 2');
  });


});

app.filter("sanitize", ['$sce', function($sce) {
  return function(htmlCode){
    return $sce.trustAsHtml(htmlCode);
  }
}]);